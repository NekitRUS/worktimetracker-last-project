Attribute VB_Name = "Subs"
Option Explicit
Option Base 0

'===========================================
'���������� ���������� �������� � ����������
'===========================================
Public Const APP_NAME As String = "��������� ""��������"""
Public Const MENU_CAPTION As String = "��������"
Public Const GLOBAL_ERR_DESCRIPTION As String = "���-�� ����� �� ���!" & vbCrLf & "���������� ��������� ��������� ������."
Public EXCEL_VERSION As Double
Public APP_DATA_PATH As String

'===========================
'��������� ������� ���������
'===========================
Public Sub Start()
    
    EXCEL_VERSION = CDbl(Replace(Application.Version, ".", ","))
    APP_DATA_PATH = Application.ThisWorkbook.path & Application.PathSeparator & _
                    MENU_CAPTION & "_App_Data" & Application.PathSeparator
                    
    Call InitialSettings
    
    If (Dir(APP_DATA_PATH, vbDirectory) = vbNullString) Then
        MkDir APP_DATA_PATH
    End If
    
    frmAuth.Show

End Sub

'======================================================
'��������� ��� ��������� �������� ������������ ��������
'======================================================
Private Sub InitialSettings()

    On Error GoTo Exception
    Dim test As String
    test = ThisWorkbook.VBProject.name
    Exit Sub
    
Exception:
    If (Not (Err.Number = 1004)) Then
        Exit Sub
    Else
        Dim answer As VbMsgBoxResult
        answer = MsgBox("��� ���������� ������ ��������� ���������� " & vbCrLf & _
                    "��������� ������ � ��������� ������ �������� VBA." & vbCrLf & _
                    "���������?", vbYesNo + vbQuestion, APP_NAME)
        If (answer = vbNo) Then
            End
        Else
            Dim regPath As String
            Dim WSHShell As Object
            regPath = "HKEY_LOCAL_MACHINE\Software\Microsoft\Office\" & _
                        Application.Version & "\Excel\Security\AccessVBOM"
            On Error GoTo -1
            Set WSHShell = CreateObject("WScript.Shell")
            WSHShell.RegWrite regPath, 1, "REG_DWORD"
            Set WSHShell = Nothing
        End If
    End If

End Sub
