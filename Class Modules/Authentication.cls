VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Authentication"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'====================================================================
'Authentication - ����� ��� ����������� ���������������������� ������
'====================================================================

Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private users As Collection

'==================
'����������� ������
'==================
Private Sub Class_Initialize()

    Set users = New Collection

End Sub

'=======================================================================================
'�������� ����� ��� ����������� ��������� ������ � ���������� ������� � �������� �������
'=======================================================================================
Public Function LastUsedRow(ByVal column As Long, ByRef sheet As Worksheet) As Long
    
    LastUsedRow = sheet.Cells(sheet.Rows.Count, column).End(xlUp).row

End Function

'===================================================================
'�������� ����� ��� �������� ������ ������������������ �������������
'===================================================================
Public Function CreateListOfUsers(ByRef sheet As Worksheet) As Collection
    
    Dim i As Long
    Dim temp As Person
    Set users = New Collection
    On Error Resume Next
    For i = 1 To LastUsedRow(1, sheet)
        With sheet
            If (Trim(.Cells(i, 1).Value) <> vbNullString) Then
                Set temp = New Person
                temp.name = .Cells(i, 1).Value
                temp.Password = .Cells(i, 2).Value
                temp.Service = .Cells(i, 3).Value
                users.Add Item:=temp, Key:=temp.name
            End If
        End With
    Next i
    
    Set temp = Nothing
    Set CreateListOfUsers = users

End Function

'=====================================================================
'�������� ����� ��� ���������� ������ ������������������ �������������
'=====================================================================
Public Sub SortUsersOnSheet(ByRef sheet As Worksheet)

    Dim lastRow As Long
    
    On Error Resume Next
    lastRow = LastUsedRow(1, sheet)
    sheet.Range(sheet.Cells(1, 1), sheet.Cells(lastRow, 3)).Sort Key1:=sheet.Cells(1, 1), _
                                                                Order1:=xlAscending, _
                                                                Header:=xlNo, _
                                                                MatchCase:=True
                                                                
End Sub

'======================================================
'�������� ����� ��� ��������� ������������ �� ��� �����
'======================================================
Public Function GetUser(ByVal userName As String) As Person

    On Error GoTo notExist
    Set GetUser = users.Item(userName)
    Exit Function
    
notExist:
    Set GetUser = New Person
    Exit Function
    
End Function

'=====================================================================================
'�������� ����� ��� ������� ������ ������������, ������� ������������������ ����������
'=====================================================================================
Public Function RegisterUser( _
                            ByVal userName As String, _
                            ByVal userPassword As String, _
                            ByRef sheet As Worksheet _
                            ) As Boolean

    Dim rowIndex As Long
    Dim randomSalt As String
    Dim resultRange As Range
    Dim coder As Crypt
    
    Set resultRange = sheet.Range( _
                                sheet.Cells(1, 1), _
                                sheet.Cells(LastUsedRow(1, sheet), 1) _
                                ).Find( _
                                    What:=userName, _
                                    LookIn:=xlFormulas, _
                                    LookAt:=xlWhole, _
                                    SearchOrder:=xlByColumns, _
                                    SearchDirection:=xlNext, _
                                    MatchCase:=False, _
                                    SearchFormat:=False _
                                    )
    If (resultRange Is Nothing) Then
        RegisterUser = False
        Exit Function
    End If
                                                                               
    rowIndex = resultRange.row
    randomSalt = GetRandomString
    Set coder = New Crypt
    sheet.Cells(rowIndex, 2).Value = coder.GetMD5(userPassword & randomSalt)
    sheet.Cells(rowIndex, 3).Value = randomSalt
    sheet.Parent.Save
    RegisterUser = True
    
    Set coder = Nothing
    Set resultRange = Nothing

End Function

'====================================================================
'�������� �����, ������������ �������������� ��������� ������� ������
'====================================================================
Private Function GetRandomString() As String

    Dim i As Integer
    Dim chars As String
    chars = "!@#$%^&*()_+�;:?-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    
    Randomize
    For i = 1 To Int(11 * Rnd + 10)
        GetRandomString = GetRandomString & Mid(chars, Int((Len(chars)) * Rnd + 1), 1)
    Next i

End Function

'=============================================
'�������� ����� ��� �������� ���������� ������
'=============================================
Public Function Verify(ByVal pass As String, ByRef user As Person) As Boolean

    Dim coder As Crypt
    Set coder = New Crypt
    If (user.Password = coder.GetMD5(pass & user.Service)) Then
        Verify = True
    Else
        Verify = False
    End If
    Set coder = Nothing

End Function

'=====================================================================
'�������� ����� ��� ���������� ������ ������������������ �������������
'=====================================================================
Public Function UpdateListOfUsers(ByRef sheet As Worksheet, ByRef book As Workbook) As Boolean

    Dim tempList As Collection
    Dim temp As Person
    Dim ws As Worksheet
    Dim resultRange As Range
    Dim fullName As String
    
    On Error Resume Next
    
    Set tempList = New Collection
    For Each ws In book.Sheets
        fullName = vbNullString
        Set resultRange = Nothing
        Set resultRange = ws.Cells.Find( _
                                        What:="��������", _
                                        LookIn:=xlFormulas, _
                                        LookAt:=xlWhole, _
                                        SearchOrder:=xlByColumns, _
                                        SearchDirection:=xlNext, _
                                        MatchCase:=False, _
                                        SearchFormat:=False _
                                        )
        If (Not (resultRange Is Nothing)) Then
            Set resultRange = ws.Cells.Find( _
                                            What:="�*�*�*", _
                                            LookIn:=xlFormulas, _
                                            LookAt:=xlWhole, _
                                            SearchOrder:=xlByColumns, _
                                            SearchDirection:=xlNext, _
                                            MatchCase:=False, _
                                            SearchFormat:=False _
                                            )
            If (Not (resultRange Is Nothing)) Then
                fullName = Trim(resultRange.Offset(0, 1).Value)
                If (fullName <> vbNullString) Then
                    Set temp = New Person
                    temp.name = fullName
                    tempList.Add Item:=temp, Key:=fullName
                End If
            End If
        End If
    Next ws
    
    If (tempList.Count = 0) Then
        UpdateListOfUsers = False
        Set tempList = Nothing
        Set temp = Nothing
        Set ws = Nothing
        Set resultRange = Nothing
        Exit Function
    End If
    
    Dim user As Person
    For Each temp In users
        Set user = Nothing
        Set user = tempList.Item(temp.name)
        If (Not (user Is Nothing)) Then
            user.Password = temp.Password
            user.Service = temp.Service
        End If
    Next temp
    
    sheet.Columns("A:C").EntireColumn.Clear
    Dim i As Long
    i = 1
    For Each temp In tempList
        sheet.Cells(i, 1).Value = temp.name
        sheet.Cells(i, 2).Value = temp.Password
        sheet.Cells(i, 3).Value = temp.Service
        i = i + 1
    Next temp
    sheet.Parent.Save
    
    Set users = tempList
    UpdateListOfUsers = True
    
    Set tempList = Nothing
    Set temp = Nothing
    Set ws = Nothing
    Set resultRange = Nothing
    Set user = Nothing

End Function
