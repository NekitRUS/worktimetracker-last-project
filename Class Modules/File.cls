VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "File"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'=================================
'File - ����� ��� ������ � �������
'=================================

Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private forbiddenCharacters As Variant
Private appWord As Object

'==================
'����������� ������
'==================
Private Sub Class_Initialize()

    forbiddenCharacters = Array("*", "\", ":", "<", ">", "?", "/", "|", Chr(34))
    On Error Resume Next
    Set appWord = GetObject(Class:="Word.Application")
    If (Err.Number <> 0) Then
        Set appWord = CreateObject("Word.Application")
    End If
    Err.Clear
    appWord.Visible = True

End Sub

'======================================================
'��������(������) ��� ��������� ������� ���������� Word
'======================================================
Public Property Get WordApplication() As Object
    
    Set WordApplication = appWord
    
End Property
'=================================
'�������� ����� ��� �������� �����
'=================================
Public Function OpenWorkbook( _
                            ByVal path As String, _
                            ByVal pass As String, _
                            Optional ByRef wasActivated As Boolean _
                            ) As Workbook

    Set OpenWorkbook = Nothing

    On Error Resume Next
    
    Dim extension As String
    extension = Right(path, 4)
    If ((extension <> ".xls") And (extension <> "xlsx") And (extension <> "xlsm") And _
        (extension <> "xlsb")) Then
        MsgBox "��������� �������� ������ � ������� Excel (*.xls, *.xlsx, *.xlsm, *.xlsb).", _
                vbOKOnly + vbCritical, APP_NAME
        Exit Function
    End If
    
    If (path = ThisWorkbook.fullName) Then
        MsgBox "���������� ������� ���� � ����������!", vbOKOnly + vbCritical, APP_NAME
        Exit Function
    End If
    
    Dim book As Workbook
    For Each book In Workbooks
        If (book.fullName = path) Then
            Set OpenWorkbook = book
            wasActivated = True
            Set book = Nothing
            Exit Function
        End If
        If ((book.name = Right(path, Len(path) - InStrRev(path, Application.PathSeparator))) And _
            (book.fullName <> path)) Then
            book.Close savechanges:=True
        End If
    Next book
    
    If (OpenWorkbook Is Nothing) Then
        Set OpenWorkbook = Workbooks.Open(Filename:=path, Password:=pass)
        wasActivated = False
    End If
    
    Set book = Nothing

End Function

'=========================================================================
'�������� ����� ��� �������� ����� (� ������, ����� ���������� ����������)
'=========================================================================
Public Function OpenWorkbookWithoutExtension( _
                                            ByVal path As String, _
                                            ByVal pass As String, _
                                            Optional ByRef wasActivated As Boolean _
                                            ) As Workbook

    Set OpenWorkbookWithoutExtension = Nothing
    
    Dim possibleExtensions As Variant
    If (EXCEL_VERSION >= 12#) Then
        possibleExtensions = Array(".xlsx", ".xlsm", ".xls", ".xlsb")
    Else
        possibleExtensions = Array(".xls")
    End If
    
    Dim extension As Variant
    For Each extension In possibleExtensions
        Set OpenWorkbookWithoutExtension = OpenWorkbook( _
                                                        path & extension, _
                                                        pass, _
                                                        wasActivated _
                                                        )
        If (Not (OpenWorkbookWithoutExtension Is Nothing)) Then
            Exit Function
        End If
    Next extension
    
End Function

'=====================================
'�������� ����� ��� �������� ���������
'=====================================
Public Function OpenDocument( _
                            ByVal path As String, _
                            Optional ByRef wasActivated As Boolean _
                            ) As Object

    Set OpenDocument = Nothing

    On Error Resume Next
    
    Dim extension As String
    extension = Right(path, 4)
    If ((extension <> ".doc") And (extension <> "docx") And (extension <> "docm") And _
        (extension <> "docb") And (extension <> ".dot") And (extension <> "dotx")) Then
        Exit Function
    End If
        
    Dim docWord As Object
    For Each docWord In appWord.Documents
        If (docWord.fullName = path) Then
            Set OpenDocument = docWord
            wasActivated = True
            Set docWord = Nothing
            Exit Function
        End If
    Next docWord
    
    If (OpenDocument Is Nothing) Then
        Set OpenDocument = appWord.Documents.Open(Filename:=path)
        wasActivated = False
    End If
    
    Set docWord = Nothing

End Function

'=============================================================================
'�������� ����� ��� �������� ��������� (� ������, ����� ���������� ����������)
'=============================================================================
Public Function OpenDocumentWithoutExtension( _
                                            ByVal path As String, _
                                            Optional ByRef wasActivated As Boolean _
                                            ) As Object

    Set OpenDocumentWithoutExtension = Nothing
    
    Dim possibleExtensions As Variant
    If (EXCEL_VERSION >= 12#) Then
        possibleExtensions = Array(".docx", ".docm", ".doc", ".docb")
    Else
        possibleExtensions = Array(".doc")
    End If
    
    Dim extension As Variant
    For Each extension In possibleExtensions
        Set OpenDocumentWithoutExtension = OpenDocument( _
                                                        path & extension, _
                                                        wasActivated _
                                                        )
        If (Not (OpenDocumentWithoutExtension Is Nothing)) Then
            Exit Function
        End If
    Next extension
    
End Function

'=============================================================
'�������� ����� ��� ������ � ����� ����� ������������ ��������
'=============================================================
Public Function CorrectPath(ByVal path As String) As String

    Dim i As Integer
    For i = 0 To UBound(forbiddenCharacters)
        path = Replace(path, forbiddenCharacters(i), "#")
    Next i
    CorrectPath = path

End Function

'=======================================================
'�������� ����� ��� �������� ������������� ����� � �����
'=======================================================
Public Function IsSheetExist( _
                            ByVal name As String, _
                            ByRef book As Workbook, _
                            Optional ByRef sheet As Worksheet _
                            ) As Boolean

    On Error Resume Next
    
    Set sheet = book.Sheets(name)
    If (sheet Is Nothing) Then
        IsSheetExist = False
    Else
        IsSheetExist = True
    End If

End Function

'===========================================
'�������� ����� ��� ���������� ����� � �����
'===========================================
Public Function AddSheet(ByVal name As String, ByRef book As Workbook) As Worksheet

    Dim sheet As Worksheet
    On Error Resume Next
    
    Set AddSheet = Nothing
    Set sheet = book.Sheets.Add
    sheet.name = name
    Set AddSheet = sheet
    
    Set sheet = Nothing

End Function

'=================================
'�������� ����� ��� �������� �����
'=================================
Public Sub CreateDirectory(ByVal fullPath As String)
    
    Dim i As Long
    Dim str As String, basePath As String, newPath As String
    Dim strArray As Variant
 
    str = fullPath
    If (Right$(str, 1) <> Application.PathSeparator) Then
        str = str & Application.PathSeparator
    End If
    strArray = Split(str, Application.PathSeparator)
    basePath = strArray(0) & Application.PathSeparator
     
    For i = 1 To UBound(strArray) - 1
        If (Len(newPath) = 0) Then
            newPath = basePath & newPath & strArray(i) & Application.PathSeparator
        Else
            newPath = newPath & strArray(i) & Application.PathSeparator
        End If
     
        If (Not (IsFolderExist(newPath))) Then
            MkDir newPath
        End If
    Next i
 
End Sub
 
'===================================================
'�������� ����� ��� �����������, ���������� �� �����
'===================================================
Private Function IsFolderExist(ByVal path As String) As Boolean

    On Error Resume Next
    IsFolderExist = ((GetAttr(path) And vbDirectory) = vbDirectory)

End Function

'===============================================================
'�������� ����� ��� ���������� ������������ ������� ������ �����
'===============================================================
Public Sub InsertWorkbookEventHandlersAndModules(ByRef book As Workbook)

    Dim VBACodeModule As Object, VBAComponent As Object
    Dim linesCount As Long
    Dim VBEOldSettings As Boolean
    
    On Error Resume Next
    
    VBEOldSettings = Application.VBE.MainWindow.Visible
    Application.VBE.MainWindow.Visible = False
    
    Set VBACodeModule = book.VBProject.VBComponents(book.CodeName).CodeModule
    With VBACodeModule
        linesCount = .ProcCountLines("Workbook_SheetDeactivate", 0)
        If (linesCount = 0) Then
            .InsertLines .CountOfLines + 1, ThisWorkbook.Sheets(1).Cells(1, 7).Value
        End If
    End With
    
    Set VBAComponent = book.VBProject.VBComponents("frmPersonalDetails")
    If (VBAComponent Is Nothing) Then
        Set VBAComponent = book.VBProject.VBComponents.Add(3)
        VBAComponent.name = "frmPersonalDetails"
        Set VBACodeModule = VBAComponent.CodeModule
        With VBACodeModule
            .InsertLines .CountOfLines + 1, ThisWorkbook.Sheets(1).Cells(1, 11).Value
        End With
    End If
    Set VBAComponent = Nothing
    
    Set VBAComponent = book.VBProject.VBComponents("cmdMyButton")
    If (VBAComponent Is Nothing) Then
        Set VBAComponent = book.VBProject.VBComponents.Add(2)
        VBAComponent.name = "cmdMyButton"
        Set VBACodeModule = VBAComponent.CodeModule
        With VBACodeModule
            .InsertLines .CountOfLines + 1, ThisWorkbook.Sheets(1).Cells(1, 10).Value
        End With
    End If
    Set VBAComponent = Nothing
    
    Set VBAComponent = book.VBProject.VBComponents("ModuleSpecial")
    If (VBAComponent Is Nothing) Then
        Set VBAComponent = book.VBProject.VBComponents.Add(1)
        VBAComponent.name = "ModuleSpecial"
        Set VBACodeModule = VBAComponent.CodeModule
        With VBACodeModule
            .InsertLines .CountOfLines + 1, ThisWorkbook.Sheets(1).Cells(1, 9).Value
        End With
    End If
        
    Application.VBE.MainWindow.Visible = VBEOldSettings
    Set VBACodeModule = Nothing
    Set VBAComponent = Nothing
    
End Sub

'===============================================================
'�������� ����� ��� ���������� ������������ ������� ������ �����
'===============================================================
Public Sub InsertSheetEventHandlers(ByRef sheet As Worksheet)

    Dim VBACodeModule As Object
    Dim linesCount As Long
    Dim VBEOldSettings As Boolean
    
    On Error Resume Next
    
    VBEOldSettings = Application.VBE.MainWindow.Visible
    Application.VBE.MainWindow.Visible = False
    
    Set VBACodeModule = sheet.Parent.VBProject.VBComponents(sheet.CodeName).CodeModule
    With VBACodeModule
        linesCount = .ProcCountLines("Worksheet_Activate", 0)
        If (linesCount = 0) Then
            .InsertLines .CountOfLines + 1, ThisWorkbook.Sheets(1).Cells(1, 8).Value
        End If
    End With
    
    Application.VBE.MainWindow.Visible = VBEOldSettings
    Set VBACodeModule = Nothing
  
End Sub

'==========================================================
'�������� ����� ��� ����������� ������ ������������ �������
'==========================================================
Public Function ListOfTimesheets(ByVal year As String, ByRef user As Person) As Collection

    Dim wasActiwated As Boolean
    Dim i As Integer
    Dim wb As Workbook
    Dim months As Variant, curMonth As Variant
    Dim tempList As Collection, monthsCollection As Collection
    
    On Error Resume Next
    
    months = Array("������", "�������", "����", "������", "���", "����", _
                    "����", "������", "��������", "�������", "������", "�������")
    Set tempList = New Collection
                    
    For i = (CInt(year) - 1) To (CInt(year) + 1)
    
        Set monthsCollection = New Collection
        Set wb = OpenWorkbookWithoutExtension(APP_DATA_PATH & user.name & Application.PathSeparator & _
                                                CStr(i) & Application.PathSeparator & _
                                                "������_" & user.name & "(" & CStr(i) & ")", _
                                                user.Password, _
                                                wasActiwated)
        If (Not (wb Is Nothing)) Then
            For Each curMonth In months
                If (IsSheetExist(curMonth, wb)) Then
                    monthsCollection.Add curMonth
                End If
            Next curMonth
            If (Not (wasActiwated)) Then
                wb.Close False
            End If
        End If
        
        tempList.Add Item:=monthsCollection, Key:=CStr(i)
    
    Next i
    
    Set ListOfTimesheets = tempList
    
    Set wb = Nothing
    Set tempList = Nothing
    Set monthsCollection = Nothing
    
End Function

'==================================================
'�������� ����� ��� �������� �� ������������ ������
'==================================================
Public Function IsSheetValid(ByRef sheet As Worksheet) As Boolean

    Dim i As Integer
    
    On Error GoTo Exception
    
    IsSheetValid = True
    For i = 1 To 28
        If (CInt(Trim(sheet.Cells(4, i + 3))) <> i) Then
            IsSheetValid = False
            Exit Function
        End If
    Next i
    
    Exit Function
    
Exception:
    IsSheetValid = False
    Exit Function
    
End Function

'=================
'���������� ������
'=================
Private Sub Class_Terminate()

    If (appWord.Documents.Count = 0) Then
        appWord.Quit
    End If
    Set appWord = Nothing

End Sub
