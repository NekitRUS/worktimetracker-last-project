VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Timesheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'======================================
'Timesheet - ����� ��� ������ � �������
'======================================

Option Explicit
Option Base 0

'=============================================
'���������� ����������
'
'READYSTATE - ��������� ������������ ���������
'=============================================
Private Enum READYSTATE
    READYSTATE_UNINITIALIZED = 0
    READYSTATE_LOADING = 1
    READYSTATE_LOADED = 2
    READYSTATE_INTERACTIVE = 3
    READYSTATE_COMPLETE = 4
End Enum

Private errDesc As String
Private mySheet As Worksheet

'====================================================================
'����� ������� �������� �����, �� ������� ����� ������������� �������
'====================================================================
Public Sub Init(ByRef sheet As Worksheet)

    Set mySheet = sheet
    errDesc = "��� ������ � ����� �������� ��������� ���������� ��������� ����������:" & vbCrLf & _
                "Microsoft XML," & vbCrLf & "Microsoft HTML Object Library."
                
End Sub

'=============================================
'��������(������) ��� ��������� �������� �����
'=============================================
Public Property Get ProcessingSheet() As Worksheet
    
    Set ProcessingSheet = mySheet
    
End Property

'==============================================================================
'�������� ����� �������� �� ��������� ��������� ���������� ���� � �������� ����
'==============================================================================
Private Function CalendarImport( _
                                ByVal year As Integer, _
                                ByVal maxDelay As Integer, _
                                Optional ByVal site$ = "http://www.superjob.ru/proizvodstvennyj_kalendar/" _
                                ) As Collection
    
    Dim XMLLibraries As Variant
    XMLLibraries = Array("MSXML2.XMLHTTP", "Microsoft.XMLHTTP", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP.6.0")
    
    Dim XMLHTTP As Object
    Dim i As Integer
    For i = 0 To UBound(XMLLibraries)
        On Error GoTo looper
        Set XMLHTTP = CreateObject(XMLLibraries(i))
        Exit For
looper:
    Resume nextI
nextI:
    Next i
    
    On Error GoTo 0
    
    If (XMLHTTP Is Nothing) Then
        Err.Raise Number:=200, Description:=errDesc
    End If
    
    XMLHTTP.Open "GET", site & CStr(year) & "/", False
    XMLHTTP.send
    Dim Start As Single
    Start = Timer

    Application.StatusBar = "���� ����������� � ����� � ����������..."
    Do While (XMLHTTP.READYSTATE <> READYSTATE_COMPLETE)
        DoEvents
        If (Timer > Start + maxDelay) Then
            Application.StatusBar = vbNullString
            Err.Raise Number:=201, Description:="�������� ����� �������� ������ �� ������� �������."
        End If
    Loop

    On Error Resume Next
    
    Dim html As Object
    Set html = CreateObject("HTMLfile")
    
    On Error GoTo 0
    
    If (html Is Nothing) Then
        Err.Raise Number:=200, Description:=errDesc
    End If
    
    html.body.innerHTML = XMLHTTP.responseText
    
    Application.StatusBar = vbNullString

    Dim allDivElements As Object, divElement As Object, monthElement As Object, dayElement As Object
    Set allDivElements = html.getElementsByTagName("div")
    
    Dim list As Collection
    Set list = New Collection
    Dim restDays() As Integer
    Dim j As Integer
    j = 0
    ReDim restDays(0 To j)
    restDays(j) = 0
    Dim monthName As String
    
    For Each divElement In allDivElements
        If (divElement.className = "MonthsList") Then
            For Each monthElement In divElement.all
                
                If (monthElement.className = "sj_h2 m_b_0 m_light h_text_align_center") Then
                    monthName = monthElement.innerText
                End If
                
                If (monthElement.className = "MonthsList_dates") Then
            
                    j = 0
                    ReDim restDays(0 To j)
                    
                    For Each dayElement In monthElement.all
                        If (dayElement.className = "MonthsList_holiday MonthsList_date") Then
                            ReDim Preserve restDays(0 To j)
                            restDays(j) = CInt(dayElement.innerText)
                            j = j + 1
                        End If
                    Next dayElement
                    
                    list.Add Item:=restDays, Key:=monthName
                
                End If
                
            Next monthElement
        End If
    Next divElement
    
    Set CalendarImport = list
    
    Set XMLHTTP = Nothing
    Set html = Nothing
    Set allDivElements = Nothing
    Set divElement = Nothing
    Set monthElement = Nothing
    Set dayElement = Nothing
    Set list = Nothing
    
End Function

'=======================================================================================
'�������� ����� ��� ����������� ��������� ������ � ���������� ������� � �������� �������
'=======================================================================================
Public Function LastUsedRow(ByVal column As Long) As Long

    LastUsedRow = mySheet.Cells(mySheet.Rows.Count, column).End(xlUp).row

End Function

'================================================================
'�������� ����� ��� ������������ ��������� ���� � �������� ������
'================================================================
Public Sub ColorHolidays(ByVal curMonth As Integer, ByVal curYear As Integer)

    Dim days As Integer
    Dim j As Long, lastRow As Long
    Dim now As Date
    
    now = CDate("1/" + CStr(curMonth) + "/" + CStr(curYear))
    days = DateSerial(year(now), month(now) + 1, 1) - DateSerial(year(now), month(now), 1)
    lastRow = LastUsedRow(2)
    
    
    For j = 4 To days + 3
    
        now = CDate(CStr(mySheet.Cells(4, j).Value) + "/" + CStr(curMonth) + "/" + CStr(curYear))
                
        If ((DatePart("w", now) = vbSaturday) Or (DatePart("w", now) = vbSunday)) Then
            mySheet.Range(mySheet.Cells(4, j), mySheet.Cells(lastRow, j)).Interior.ColorIndex = 46
        End If
        
    Next j

End Sub

'==================================================================================================
'�������� ����� ��� ������������ ��������� ���� � �������� ������ (� ��������� ������ �� ���������)
'==================================================================================================
Public Sub ColorHolidaysWithInternet( _
                                    ByVal monthNumber As Integer, _
                                    ByVal curMonth As String, _
                                    ByVal curYear As Integer, _
                                    ByVal maxDelay As Integer _
                                    )

    Dim i As Integer, days As Integer
    Dim j As Long, lastRow As Long
    Dim now As Date
    Dim weekends As Collection
    
    now = CDate("1/" + CStr(monthNumber) + "/" + CStr(curYear))
    days = DateSerial(year(now), month(now) + 1, 1) - DateSerial(year(now), month(now), 1)
    lastRow = LastUsedRow(2)
    Set weekends = CalendarImport(curYear, maxDelay)
    
    For j = 4 To days + 3
    
        For i = LBound(weekends(curMonth)) To UBound(weekends(curMonth))
        
            If (mySheet.Cells(4, j).Value = weekends(curMonth)(i)) Then
                mySheet.Range(mySheet.Cells(4, j), mySheet.Cells(lastRow, j)).Interior.ColorIndex = 46
            End If
            
        Next i
        
    Next j

End Sub

'==========================================================================
'�������� ����� ��� �������� ������ ���-�� ������������ ����� � �����������
'==========================================================================
Public Sub SummUpHours()

    Dim i As Long
    Dim j As Integer, sum As Integer, over As Integer, absence As Integer, _
        vacation As Integer, vacationNonPaid As Integer, disability As Integer
    Dim curValue As Variant
    
    mySheet.Range(mySheet.Cells(5, 35), mySheet.Cells(LastUsedRow(2) + 1, 40)).ClearContents
    
    For i = 5 To LastUsedRow(2)
        
        With mySheet
        
            sum = 0
            over = 0
            absence = 0
            vacation = 0
            vacationNonPaid = 0
            disability = 0
            
            For j = 4 To 34
nextJ:
                curValue = .Cells(i, j).Value
                If (LCase(Trim(curValue)) = "��") Then
                    vacation = vacation + 1
                    j = j + 1
                    GoTo nextJ
                ElseIf (LCase(Trim(curValue)) = "��") Then
                    absence = absence + 1
                    j = j + 1
                    GoTo nextJ
                ElseIf (LCase(Trim(curValue)) = "��") Then
                    vacationNonPaid = vacationNonPaid + 1
                    j = j + 1
                    GoTo nextJ
                ElseIf (LCase(Trim(curValue)) = "�") Then
                    disability = disability + 1
                    j = j + 1
                    GoTo nextJ
                ElseIf (Not (IsNumeric(curValue))) Then
                    j = j + 1
                    GoTo nextJ
                End If
                
                sum = sum + curValue
            
                If ((curValue > 8) And _
                    ((.Cells(4, j).Interior.ColorIndex = 2) Or _
                    (.Cells(4, j).Interior.ColorIndex = xlNone))) Then
                    over = over + (curValue - 8)
                ElseIf ((.Cells(4, j).Interior.ColorIndex <> 2) And _
                    (.Cells(4, j).Interior.ColorIndex <> xlNone)) Then
                    over = over + curValue
                End If
                
            Next j
            
            If (Trim(.Cells(i, 2).Value) <> vbNullString) Then
                .Cells(i, 35).Value = sum
                .Cells(i, 36).Value = over
                .Cells(i, 37).Value = absence * 8
                .Cells(i, 38).Value = vacation * 8
                .Cells(i, 39).Value = vacationNonPaid * 8
                .Cells(i, 40).Value = disability * 8
            End If
        
        End With
        
    Next i
    
    MsgBox "��������� ���� �� ����� ������� ����������!", vbInformation + vbOKOnly, APP_NAME

End Sub

'===========================================================================
'�������� ����� ��� ��������� ���������� � ���������� ������ ����� �� ������
'===========================================================================
Public Sub DecorateAndPrepareForPrint( _
                                    ByVal month As Integer, _
                                    ByVal strMonth As String, _
                                    ByVal year As Integer, _
                                    ByVal sign As String _
                                    )

    Dim oldCalculating As XlCalculation
    oldCalculating = Application.Calculation
    Application.Calculation = xlCalculationManual
    
    On Error Resume Next
    
    With mySheet
    
        .Range(.Cells(1, 1), .Cells(4, 40)).MergeCells = False
        .Range(.Cells(1, 1), .Cells(4, 40)).Clear
        .Range(.Cells(1, 1), .Cells(2, 40)).MergeCells = True
        .Range(.Cells(3, 1), .Cells(3, 40)).MergeCells = True
        
        .Cells(1, 1).Value = "�������� � ������������ ���������������� ���������������-������������ ����� " _
                                & Chr(34) & "�����-�" & Chr(34)
        .Cells(3, 1).Value = "������ ����� �������� ������� �� " + LCase(strMonth) + " " + CStr(year) + "�."
        .Cells(4, 1).Value = "� �/�"
        .Cells(4, 2).Value = "���"
        .Cells(4, 3).Value = "���������"
        Dim i As Integer
        For i = 4 To (countDays(month, year) + 3)
            .Cells(4, i).Value = CStr(i - 3)
        Next i
        .Cells(4, 35).Value = "����� �����," & vbLf & "������������" & vbLf & "�� �����, �"
        .Cells(4, 36).Value = "�����," & vbLf & "������������" & vbLf & "�����������, �"
        .Cells(4, 37).Value = "�������, �"
        .Cells(4, 38).Value = "���������" & vbLf & "������������" & vbLf & "������, �"
        .Cells(4, 39).Value = "������ ���" & vbLf & "����������" & vbLf & "����������" & vbLf & "�����, �"
        .Cells(4, 40).Value = "���������" & vbLf & "������������������," & vbLf & "�"
        
        With .Cells
            .RowHeight = 18
            .Borders.LineStyle = xlNone
            .Interior.pattern = xlNone
            .WrapText = False
            .ShrinkToFit = False
            With .Font
                .name = "Times New Roman"
                .size = 12
            End With
        End With

        With .Range(.Cells(1, 1), .Cells(4, 40))
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Font.Bold = True
        End With
        With .Cells(1, 1).Font
            .size = 14
            .Italic = True
        End With
        .Cells(3, 1).Font.size = 20
        With .Columns(1)
            .ColumnWidth = 6.5
            .VerticalAlignment = xlCenter
        End With
        With .Columns(2)
            .WrapText = True
            .ColumnWidth = 23
            .VerticalAlignment = xlCenter
        End With
        With .Columns(3)
            .ColumnWidth = 15
            .VerticalAlignment = xlCenter
        End With
        .Range(.Cells(4, 4), .Cells(4, 34)).ColumnWidth = 3
        With .Range(.Cells(4, 35), .Cells(4, 40))
            .WrapText = True
            .ColumnWidth = 16
        End With
        .Columns(40).ColumnWidth = 21

        With .Range(.Cells(4, 1), .Cells(LastUsedRow(2), 40)).Borders
            .ColorIndex = 1
            .LineStyle = xlContinuous
            .Weight = xlThin
        End With
        .Rows("1:" & CStr(LastUsedRow(2))).EntireRow.AutoFit
        
        Dim selectedSheet As Worksheet
        Set selectedSheet = ActiveSheet
        .Activate
        With ActiveWindow
            .Zoom = 78
            .ScrollColumn = 1
            .ScrollRow = 1
        End With
        selectedSheet.Activate
        Set selectedSheet = Nothing
        
        '��� ��������� ������ ��������� ����� ���������� �������� ������������ ��� ����������
        '� �������� �� ������������� ���������
        With .PageSetup
            If (.PrintArea <> _
                mySheet.Range(mySheet.Cells(1, 1), mySheet.Cells(LastUsedRow(2), 40)).Address) Then
                .PrintArea = mySheet.Range(mySheet.Cells(1, 1), mySheet.Cells(LastUsedRow(2), 40)).Address
            End If
            If (EXCEL_VERSION >= 14#) Then
                Application.PrintCommunication = False
            End If
            Dim leftFooterText As String, rightFooterText As String
            If (Len(sign) > 60) Then
                sign = Space(10)
            End If
            leftFooterText = "&13������������� ���� &U   ��������   &U" & Space(3) & "&U" & Space(25) & "&U" & _
                            Space(3) & "&U" & Space(3) & sign & Space(30)
            rightFooterText = "&13""__""" & "________" & " 20__�."
            If (.PrintTitleRows <> mySheet.Rows(4).Address) Then .PrintTitleRows = mySheet.Rows(4).Address
            If (.PrintTitleColumns <> vbNullString) Then .PrintTitleColumns = vbNullString
            If (.LeftHeader <> vbNullString) Then .LeftHeader = vbNullString
            If (.CenterHeader <> vbNullString) Then .CenterHeader = vbNullString
            If (.RightHeader <> vbNullString) Then .RightHeader = vbNullString
            If (.LeftFooter <> leftFooterText) Then .LeftFooter = leftFooterText
            If (.CenterFooter <> vbNullString) Then .CenterFooter = vbNullString
            If (.RightFooter <> rightFooterText) Then .RightFooter = rightFooterText
            If (EXCEL_VERSION >= 12#) Then
                If (.EvenPage.LeftHeader.Text <> vbNullString) Then .EvenPage.LeftHeader.Text = vbNullString
                If (.EvenPage.CenterHeader.Text <> vbNullString) Then .EvenPage.CenterHeader.Text = vbNullString
                If (.EvenPage.RightHeader.Text <> vbNullString) Then .EvenPage.RightHeader.Text = vbNullString
                If (.EvenPage.LeftFooter.Text <> leftFooterText) Then .EvenPage.LeftFooter.Text = leftFooterText
                If (.EvenPage.CenterFooter.Text <> vbNullString) Then .EvenPage.CenterFooter.Text = vbNullString
                If (.EvenPage.RightFooter.Text <> rightFooterText) Then .EvenPage.RightFooter.Text = rightFooterText
                If (.FirstPage.LeftHeader.Text <> vbNullString) Then .FirstPage.LeftHeader.Text = vbNullString
                If (.FirstPage.CenterHeader.Text <> vbNullString) Then .FirstPage.CenterHeader.Text = vbNullString
                If (.FirstPage.RightHeader.Text <> vbNullString) Then .FirstPage.RightHeader.Text = vbNullString
                If (.FirstPage.LeftFooter.Text <> leftFooterText) Then .FirstPage.LeftFooter.Text = leftFooterText
                If (.FirstPage.CenterFooter.Text <> vbNullString) Then .FirstPage.CenterFooter.Text = vbNullString
                If (.FirstPage.RightFooter.Text <> rightFooterText) Then .FirstPage.RightFooter.Text = rightFooterText
            End If
            Dim margin As Single
            margin = Application.InchesToPoints(0.19685)
            If (.LeftMargin <> margin) Then .LeftMargin = margin
            If (.RightMargin <> margin) Then .RightMargin = margin
            If (.TopMargin <> 0) Then .TopMargin = 0
            If (.HeaderMargin <> 0) Then .HeaderMargin = 0
            If (.FooterMargin <> margin) Then .FooterMargin = margin
            If (.BottomMargin <> Application.InchesToPoints(0.590551181102362)) Then _
                                            .BottomMargin = Application.InchesToPoints(0.590551181102362)
            If (.PrintHeadings <> False) Then .PrintHeadings = False
            If (.PrintGridlines <> False) Then .PrintGridlines = False
            If (.PrintComments <> xlPrintNoComments) Then .PrintComments = xlPrintNoComments
            If (.Draft <> False) Then .Draft = False
            If (.PaperSize <> xlPaperA4) Then .PaperSize = xlPaperA4
            If (.Orientation <> xlLandscape) Then .Orientation = xlLandscape
            If (.CenterHorizontally <> True) Then .CenterHorizontally = True
            If (.CenterVertically <> True) Then .CenterVertically = True
            If (.FirstPageNumber <> xlAutomatic) Then .FirstPageNumber = xlAutomatic
            If (.Order <> xlDownThenOver) Then .Order = xlDownThenOver
            If (.BlackAndWhite <> False) Then .BlackAndWhite = False
            If (.Zoom <> False) Then .Zoom = False
            If (.FitToPagesWide <> 1) Then .FitToPagesWide = 1
            If (.FitToPagesTall <> False) Then .FitToPagesTall = False
            If (.PrintErrors <> xlPrintErrorsBlank) Then .PrintErrors = xlPrintErrorsBlank
            If (EXCEL_VERSION >= 12#) Then
                If (.OddAndEvenPagesHeaderFooter <> False) Then .OddAndEvenPagesHeaderFooter = False
                If (.DifferentFirstPageHeaderFooter <> False) Then .DifferentFirstPageHeaderFooter = False
                If (.ScaleWithDocHeaderFooter <> True) Then .ScaleWithDocHeaderFooter = True
                If (.AlignMarginsHeaderFooter <> True) Then .AlignMarginsHeaderFooter = True
            End If
            If (EXCEL_VERSION >= 14#) Then
                Application.PrintCommunication = True
            End If
        End With
        
    End With
    
    Application.Calculation = oldCalculating
    
End Sub

'==========================================================
'�������� ����� ��� �����������, �������� �� ��� ����������
'==========================================================
Private Function IsLeap(ByVal year As Long) As Boolean

    IsLeap = ((year Mod 4 = 0) And (year Mod 100 <> 0)) Or (year Mod 400 = 0)

End Function

'====================================================
'�������� ����� ��� �������� ���������� ���� � ������
'====================================================
Private Function countDays(ByVal month As Integer, ByVal year As Integer) As Integer

    Select Case month
        Case 1, 3, 5, 7, 8, 10, 12:
            countDays = 31
        Case 4, 6, 9, 11:
            countDays = 30
        Case 2:
            If (IsLeap(year)) Then
                countDays = 29
            Else
                countDays = 28
            End If
    End Select

End Function

'==========================================
'�������� ����� ��� �������� ������ �������
'==========================================
Public Function LoadListOfWorkers(ByVal brigadier As String, ByRef source As Workbook) As Boolean

    Dim tempList As Collection
    Dim temp As Person
    Dim ws As Worksheet
    Dim resultRange As Range
    Dim fullName As String
    
    On Error Resume Next
    
    Set tempList = New Collection
    For Each ws In source.Sheets
        fullName = vbNullString
        Set resultRange = Nothing
        Set resultRange = ws.Cells.Find( _
                                        What:="� �������", _
                                        LookIn:=xlFormulas, _
                                        LookAt:=xlWhole, _
                                        SearchOrder:=xlByColumns, _
                                        SearchDirection:=xlNext, _
                                        MatchCase:=False, _
                                        SearchFormat:=False _
                                        )
        If (Not (resultRange Is Nothing)) Then
            If (Trim(resultRange.Offset(0, 1).Value) = brigadier) Then
                Set resultRange = ws.Cells.Find( _
                                                What:="�*�*�*", _
                                                LookIn:=xlFormulas, _
                                                LookAt:=xlWhole, _
                                                SearchOrder:=xlByColumns, _
                                                SearchDirection:=xlNext, _
                                                MatchCase:=False, _
                                                SearchFormat:=False _
                                                )
                If (Not (resultRange Is Nothing)) Then
                    fullName = Trim(resultRange.Offset(0, 1).Value)
                    If (fullName <> vbNullString) Then
                        Set temp = New Person
                        Set resultRange = ws.Cells.Find( _
                                                What:="���������", _
                                                LookIn:=xlFormulas, _
                                                LookAt:=xlWhole, _
                                                SearchOrder:=xlByColumns, _
                                                SearchDirection:=xlNext, _
                                                MatchCase:=False, _
                                                SearchFormat:=False _
                                                )
                        If (Not (resultRange Is Nothing)) Then
                            temp.Service = Trim(resultRange.Offset(0, 1).Value)
                        End If
                        temp.name = fullName
                        tempList.Add Item:=temp, Key:=fullName
                    End If
                End If
            End If
        End If
    Next ws
    
    If (tempList.Count = 0) Then
        Set tempList = Nothing
        Set temp = Nothing
        Set ws = Nothing
        Set resultRange = Nothing
        LoadListOfWorkers = False
        Exit Function
    End If
    
    With mySheet
        .Range(.Cells(5, 1), .Cells(LastUsedRow(2), 3)).Clear
    End With
    Dim i As Long
    i = 1
    For Each temp In tempList
        With mySheet
            .Cells(i + 4, 1).Value = i
            .Cells(i + 4, 2).Value = temp.name
            .Cells(i + 4, 3).Value = temp.Service
        End With
        i = i + 1
    Next temp
    
    Set tempList = Nothing
    Set temp = Nothing
    Set ws = Nothing
    Set resultRange = Nothing
    LoadListOfWorkers = True

End Function
