VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Report"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'===================================
'Report - ����� ��� ������ � �������
'===================================

Option Explicit
Option Base 0

'================================================================
'�������� ����� ��� �������� ������ �� ����� ������ � ���� ������
'================================================================
Public Sub CreateReport( _
                        ByRef sheet As Worksheet, _
                        ByVal curMonth As String, _
                        ByVal curYear As String, _
                        ByVal author As String _
                        )

    Dim wasActivated As Boolean
    Dim fileWorker As File
    Dim docTemplate As Object, doc As Object, appWord As Object
        
    On Error Resume Next
    
    Set fileWorker = New File
    fileWorker.CreateDirectory sheet.Parent.path & Application.PathSeparator & "������"
    Set docTemplate = fileWorker.OpenDocument(APP_DATA_PATH & "�����.dot", wasActivated)
    If (docTemplate Is Nothing) Then
        MsgBox "���������� ������� �����!" & vbCrLf & "����������� ������ ������.", _
                vbOKOnly + vbCritical, APP_NAME
        Set fileWorker = Nothing
        Exit Sub
    Else
        docTemplate.Close False
        Set docTemplate = Nothing
    End If
    
    DoEvents
    
    Set appWord = fileWorker.WordApplication
    Set doc = appWord.Documents.Add(Template:=APP_DATA_PATH & "�����.dot")
    Dim monthWorker As timesheet
    Set monthWorker = New timesheet
    monthWorker.Init sheet
    Dim tableGeneral As Object, tableHooky As Object, row As Object
    Dim hookyCount As Variant
    Dim i As Long, generalNumber As Long, hookyNumber As Long
    Dim oldScreenUpdating As Boolean
    oldScreenUpdating = appWord.screenUpdating
    appWord.screenUpdating = False
    Set tableGeneral = doc.Tables(1)
    Set tableHooky = doc.Tables(2)
    For i = 5 To monthWorker.LastUsedRow(2)
        With sheet
            If (Trim(.Cells(i, 2).Value) <> vbNullString) Then
                generalNumber = generalNumber + 1
                Set row = tableGeneral.Rows.Add
                row.Cells(1).Range.Text = generalNumber
                row.Cells(2).Range.Text = .Cells(i, 2).Value
                row.Cells(3).Range.Text = .Cells(i, 3).Value
                row.Cells(4).Range.Text = .Cells(i, 35).Value
                row.Cells(5).Range.Text = .Cells(i, 36).Value
                row.Cells(7).Range.Text = .Cells(i, 38).Value
                row.Cells(8).Range.Text = .Cells(i, 39).Value
                row.Cells(9).Range.Text = .Cells(i, 40).Value
                hookyCount = .Cells(i, 37).Value
                row.Cells(6).Range.Text = hookyCount
                If (IsNumeric(hookyCount)) Then
                    If (hookyCount > 0) Then
                        Set row = tableHooky.Rows.Add
                        hookyNumber = hookyNumber + 1
                        row.Cells(1).Range.Text = hookyNumber
                        row.Cells(2).Range.Text = .Cells(i, 2).Value
                        row.Cells(3).Range.Text = .Cells(i, 3).Value
                        row.Cells(4).Range.Text = hookyCount / 8
                    End If
                End If
            End If
        End With
    Next i
    tableGeneral.Rows(2).Delete
    tableHooky.Rows(2).Delete
    Dim replaceWhat As Variant, replaceWith As Variant
    Dim monthWithEnding As String
    monthWithEnding = curMonth
    If ((Right(curMonth, 1) = "�") Or (Right(curMonth, 1) = "�")) Then
        monthWithEnding = Left(monthWithEnding, Len(monthWithEnding) - 1)
    End If
    monthWithEnding = monthWithEnding & "�"
    replaceWhat = Array("{month}", "{monthWithEnding}", "{year}", "{empNumber}", "{brigadier}")
    replaceWith = Array(curMonth, monthWithEnding, curYear, CStr(tableGeneral.Rows.Count - 1), author)
    For i = 0 To UBound(replaceWhat)
        With doc.Range.Find
            .ClearFormatting
            .Text = replaceWhat(i)
            With .Replacement
                .ClearFormatting
                .Text = LCase(replaceWith(i))
            End With
            .Execute Replace:=2, Forward:=True, Wrap:=1 '2 - wdReplaceAll, 1 - wdFindContinue
        End With
    Next i
    
    Dim chartTotal As Chart
    Dim linesCount As Long
    Set chartTotal = sheet.ChartObjects.Add(0, 0, 700, 425).Chart
    linesCount = monthWorker.LastUsedRow(2)
    With sheet
        chartTotal.SetSourceData source:=Union(.Range(.Cells(linesCount, 2), .Cells(5, 2)), _
                                                .Range(.Cells(linesCount, 35), .Cells(5, 35))), _
                                PlotBy:=xlColumns
    End With
    With chartTotal
        .ChartType = xlBarClustered
        .HasTitle = True
        .ChartTitle.Characters.Text = "����������� ����� ������ �����������"
        .HasLegend = False
        With .Axes(xlValue)
            .HasTitle = True
            .AxisTitle.Caption = "����"
        End With
        With .Axes(xlCategory)
            .HasTitle = False
            If (EXCEL_VERSION >= 12#) Then
                .TickLabels.Font.size = 14
            Else
                .TickLabels.Font.size = 10
            End If
        End With
        .ChartGroups(1).GapWidth = 50
        With .Parent
            .Width = 700
            .Height = generalNumber * 30
        End With
        .CopyPicture Appearance:=xlScreen, Format:=xlPicture, size:=xlScreen
        With doc
            .InlineShapes(1).Range.Select
            .Activate
            .ActiveWindow.Selection.Paste
        End With
        .Parent.Delete
    End With
    Set chartTotal = Nothing
        
    doc.SaveAs Filename:=sheet.Parent.path & Application.PathSeparator & "������" & _
                        Application.PathSeparator & curMonth & ".doc", _
                FileFormat:=0
    MsgBox "����� ������� ���������!", vbInformation + vbOKOnly, APP_NAME
    appWord.screenUpdating = oldScreenUpdating
    appWord.Activate
    
    Set tableGeneral = Nothing
    Set tableHooky = Nothing
    Set row = Nothing
    
End Sub
