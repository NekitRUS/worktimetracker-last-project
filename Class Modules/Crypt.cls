VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Crypt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'=======================================
'Crypt - ����� ��� ���������� MD5 ������
'=======================================

Option Explicit
Option Base 0

'=====================================
'���������� �������� � ������� �������
'=====================================
Private Const MS_DEFAULT_PROVIDER           As String = "Microsoft Base Cryptographic Provider v1.0"
Private Const PROV_RSA_FULL                 As Long = 1
Private Const CRYPT_VERIFYCONTEXT           As Long = &HF0000000
Private Const HP_HASHVAL                    As Long = 2
Private Const HP_HASHSIZE                   As Long = 4
Private Const HASH_ALGORITHM                As Long = &H8003&

Private Declare Function CryptAcquireContext _
                                            Lib "Advapi32" _
                                            Alias "CryptAcquireContextW" ( _
                                                                        phProv As Long, _
                                                                        ByVal pszContainer As Long, _
                                                                        ByVal pszProvider As Long, _
                                                                        ByVal dwProvType As Long, _
                                                                        ByVal dwFlags As Long _
                                                                        ) As Long
Private Declare Function CryptReleaseContext Lib "Advapi32" ( _
                                                            ByVal hProv As Long, _
                                                            ByVal dwFlags As Long _
                                                            ) As Long
Private Declare Function CryptCreateHash Lib "Advapi32" ( _
                                                        ByVal hProv As Long, _
                                                        ByVal AlgId As Long, _
                                                        ByVal hKey As Long, _
                                                        ByVal dwFlags As Long, _
                                                        phHash As Long _
                                                        ) As Long
Private Declare Function CryptHashData Lib "Advapi32" ( _
                                                    ByVal hHash As Long, _
                                                    pbData As Any, _
                                                    ByVal dwDataLen As Long, _
                                                    ByVal dwFlags As Long _
                                                    ) As Long
Private Declare Function CryptDestroyHash Lib "Advapi32" (ByVal hHash As Long) As Long
Private Declare Function CryptGetHashParam Lib "Advapi32" ( _
                                                        ByVal hHash As Long, _
                                                        ByVal dwParam As Long, _
                                                        pbData As Any, _
                                                        pdwDataLen As Long, _
                                                        ByVal dwFlags As Long _
                                                        ) As Long

'============================================================================
'�������� ����� ��� ���������� MD5 � ������� MS Cryptography Service Provider
'============================================================================
Private Function GetMD5ByMSCSP(message() As Byte) As String

    Dim baseProvider As Long, hash As Long, size As Long, i As Long
    Dim buffer() As Byte
    Dim success As Boolean
    
    On Error GoTo Exception
    
    success = False
    If (CryptAcquireContext( _
                            baseProvider, _
                            0, _
                            StrPtr(MS_DEFAULT_PROVIDER), _
                            PROV_RSA_FULL, _
                            CRYPT_VERIFYCONTEXT _
                            ) <> 0) Then
        If (CryptCreateHash(baseProvider, HASH_ALGORITHM, 0, 0, hash) <> 0) Then
            If (CryptHashData(hash, message(0), UBound(message) + 1, 0) <> 0) Then
                If (CryptGetHashParam(hash, HP_HASHSIZE, size, 4, 0) <> 0) Then
                    ReDim buffer(0 To size - 1) As Byte
                    If (CryptGetHashParam(hash, HP_HASHVAL, buffer(0), size, 0) <> 0) Then
                        success = True
                        For i = 0 To UBound(buffer)
                           GetMD5ByMSCSP = GetMD5ByMSCSP & LCase(Right("0" & Hex(buffer(i)), 2))
                        Next i
                    End If
                End If
            End If
            Call CryptDestroyHash(hash)
        End If
        Call CryptReleaseContext(baseProvider, 0)
    End If
    
    If (Not (success)) Then
        GoTo Exception
    End If
    
    Exit Function
    
Exception:
    Err.Raise _
            Number:=202, _
            Description:="�� ������� ��������� ��� � ������� MS Cryptography Service Provider."
    Exit Function
End Function

'================================================
'�������� ����� ��� ���������� MD5 � ������� .NET
'================================================
Private Function GetMD5ByDotNET(ByVal message As String) As String

    Dim coder As Object, encoding As Object
    Dim bytes() As Byte
    Dim i As Long
    
    On Error GoTo Exception
    
    Set encoding = CreateObject("System.Text.UTF8Encoding")
    Set coder = CreateObject("System.Security.Cryptography.MD5CryptoServiceProvider")
    
    bytes = encoding.GetBytes_4(message)
    bytes = coder.ComputeHash_2((bytes))
    
    For i = 0 To UBound(bytes)
        GetMD5ByDotNET = GetMD5ByDotNET & LCase(Right("0" & Hex(bytes(i)), 2))
    Next i
    
    Set encoding = Nothing
    Set coder = Nothing
    
    Exit Function
    
Exception:
    Err.Raise Number:=203, Description:="�� ������� ��������� ��� � ������� .NET."
    Exit Function
End Function

'=================================
'�������� ����� ��� ���������� MD5
'=================================
Public Function GetMD5(ByVal message As String) As String
    
    On Error Resume Next
    
    GetMD5 = GetMD5ByDotNET(StrReverse(GetMD5ByDotNET(message)))
    If (GetMD5 <> vbNullString) Then
        Exit Function
    End If
    
    On Error GoTo Exception
    GetMD5 = GetMD5ByMSCSP(StrConv((GetMD5ByMSCSP(StrConv(message, vbFromUnicode))), vbFromUnicode))
    Exit Function
    
Exception:
    Select Case Err.Number
        Case 202, 203
            MsgBox "��������� ���������� ���� �� ��������� ���������:" & vbCrLf & _
                    "Advanced Windows 32 Base API (advapi32.dll)," & vbCrLf & _
                    "Base Class Library (.NET Framework)." & vbCrLf & _
                    "����� ���������� ����� �� ���������.", vbCritical + vbOKOnly, APP_NAME
        Case Else
            MsgBox GLOBAL_ERR_DESCRIPTION, vbCritical + vbOKOnly, APP_NAME
    End Select
    End
End Function
