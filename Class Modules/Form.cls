VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'==========================================
'Form - ����� ��� ������ � ���������� �����
'==========================================

Option Explicit
Option Base 0

'=================================================================
'�������� ����� ��� ������� ����� ���������� �������� � ����������
'=================================================================
Public Sub OnlyDigits(ByVal KeyAscii As MSForms.ReturnInteger)

    If ((KeyAscii < 48) Or (KeyAscii > 57)) Then
        KeyAscii = 0
    End If

End Sub

'================================================================
'�������� ����� ��� ����������� �� ������������ �������� ��������
'================================================================
Public Sub MaxValue(ByVal max As Long, ByRef txtField As MSForms.TextBox)

    Dim newValue As String
    newValue = txtField.Text
    If (IsNumeric(newValue)) Then
        If (CLng(newValue) > max) Then
            txtField.Text = txtField.Tag
        Else
            txtField.Tag = txtField.Text
        End If
    Else
        txtField.Text = 0
    End If

End Sub
