VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "MyControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'============================================================================
'MyControl - ����� ��� ������������� ���������� ��������� ���������� �� �����
'============================================================================

Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Public WithEvents txtField1  As MSForms.TextBox
Attribute txtField1.VB_VarHelpID = -1
Public WithEvents txtField2  As MSForms.TextBox
Attribute txtField2.VB_VarHelpID = -1
Public WithEvents txtField3  As MSForms.TextBox
Attribute txtField3.VB_VarHelpID = -1
Public WithEvents spnField As MSForms.SpinButton
Attribute spnField.VB_VarHelpID = -1
Public WithEvents fraField As MSForms.Frame
Attribute fraField.VB_VarHelpID = -1
Public WithEvents optField1 As MSForms.OptionButton
Attribute optField1.VB_VarHelpID = -1
Public WithEvents optField2 As MSForms.OptionButton
Attribute optField2.VB_VarHelpID = -1
Public WithEvents optField3 As MSForms.OptionButton
Attribute optField3.VB_VarHelpID = -1
Public WithEvents optField4 As MSForms.OptionButton
Attribute optField4.VB_VarHelpID = -1
Public WithEvents optField5 As MSForms.OptionButton
Attribute optField5.VB_VarHelpID = -1

'=====================================================================
'���������� ������� ��������� ��������
'
'����� �������� �������� ������������ � ��������������� ��������� ����
'=====================================================================
Private Sub spnField_Change()

    txtField3.Text = spnField.Value
    
End Sub

'======================================================
'���������� ������� ������� ������� � ��������� ����
'
'����������� ����������� �� ���� ������ �������� ������
'======================================================
Private Sub txtField3_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

    Dim formWorker As Form
    Set formWorker = New Form
    formWorker.OnlyDigits KeyAscii
    Set formWorker = Nothing

End Sub

'=================================================================
'���������� ������� ��������� �������� � ��������� ����
'
'����������� ����������� �� ���� ����������� ����������� ��������,
'�������� ������� ���� ����������� � ��������� "���",
'����� �������� ������������ � ��������������� �������,
'����� �������� ������������ � ��������������� ������ �� �����
'=================================================================
Private Sub txtField3_Change()
    
    If (Not (frmFillTimesheet.EnableEvents)) Then
        Exit Sub
    End If
    
    Dim formWorker As Form
    Set formWorker = New Form
    frmFillTimesheet.EnableEvents = False
    formWorker.MaxValue 24, txtField3
    frmFillTimesheet.EnableEvents = True
    Set formWorker = Nothing
    
    optField1.Value = True
    
    frmFillTimesheet.EnableEvents = False
    spnField.Value = txtField3.Value
    frmFillTimesheet.EnableEvents = True
    
    frmFillTimesheet.curSheet.Cells(Right(txtField3.name, _
                                        Len(txtField3.name) - InStrRev(txtField3.name, "_")), _
                                    frmFillTimesheet.curDay + 3).Value = txtField3.Text
    
End Sub

'======================================================================================
'���������� ������� ��������� �������� �������������
'
'���� ������������� �������,
'�� � ������ �� ������� ����� ������������ �������� �� ���������������� ���������� ����
'======================================================================================
Private Sub optField1_Change()

    If (optField1.Value) Then
        frmFillTimesheet.curSheet.Cells(Right(optField1.name, _
                                            Len(optField1.name) - InStrRev(optField1.name, "_")), _
                                        frmFillTimesheet.curDay + 3).Value = txtField3.Text
    End If

End Sub

'===================================================================================
'���������� ������� ��������� �������� �������������
'
'���� ������������� �������, �� � ������ �� ������� ����� ������������ �������� "��"
'===================================================================================
Private Sub optField2_Change()

    If (optField2.Value) Then
        frmFillTimesheet.curSheet.Cells(Right(optField2.name, _
                                            Len(optField2.name) - InStrRev(optField2.name, "_")), _
                                        frmFillTimesheet.curDay + 3).Value = "��"
    End If

End Sub

'===================================================================================
'���������� ������� ��������� �������� �������������
'
'���� ������������� �������, �� � ������ �� ������� ����� ������������ �������� "��"
'===================================================================================
Private Sub optField3_Change()

    If (optField3.Value) Then
        frmFillTimesheet.curSheet.Cells(Right(optField3.name, _
                                            Len(optField3.name) - InStrRev(optField3.name, "_")), _
                                        frmFillTimesheet.curDay + 3).Value = "��"
    End If

End Sub

'===================================================================================
'���������� ������� ��������� �������� �������������
'
'���� ������������� �������, �� � ������ �� ������� ����� ������������ �������� "��"
'===================================================================================
Private Sub optField4_Change()

    If (optField4.Value) Then
        frmFillTimesheet.curSheet.Cells(Right(optField4.name, _
                                            Len(optField4.name) - InStrRev(optField4.name, "_")), _
                                        frmFillTimesheet.curDay + 3).Value = "��"
    End If

End Sub

'===================================================================================
'���������� ������� ��������� �������� �������������
'
'���� ������������� �������, �� � ������ �� ������� ����� ������������ �������� "�"
'===================================================================================
Private Sub optField5_Change()

    If (optField5.Value) Then
        frmFillTimesheet.curSheet.Cells(Right(optField5.name, _
                                            Len(optField5.name) - InStrRev(optField5.name, "_")), _
                                        frmFillTimesheet.curDay + 3).Value = "�"
    End If

End Sub

