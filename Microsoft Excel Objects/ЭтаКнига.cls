VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "��������"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Private Sub Workbook_Open()

    Dim myMenu As CommandBarPopup
    
    Set myMenu = Application.CommandBars("Worksheet Menu Bar").FindControl(Tag:=MENU_CAPTION)
    If (Not (myMenu Is Nothing)) Then
        Exit Sub
    End If
    
    Set myMenu = Application.CommandBars("Worksheet Menu Bar").Controls.Add(Type:=msoControlPopup)
    With myMenu
        .Caption = MENU_CAPTION
        .Tag = MENU_CAPTION
    End With
    
    With myMenu.Controls.Add(Type:=msoControlButton)
        .Caption = "�����"
        .Style = msoButtonIconAndCaption
        .FaceId = 3874
        .OnAction = "Subs.Start"
    End With

End Sub


Private Sub Workbook_BeforeClose(Cancel As Boolean)

    Dim myMenu As CommandBarPopup
    Set myMenu = Application.CommandBars("Worksheet Menu Bar").FindControl(Tag:=MENU_CAPTION)
    If (Not (myMenu Is Nothing)) Then
        myMenu.Delete
    End If
    
End Sub
