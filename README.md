Приложение предназначено для автоматизированного ведения учета рабочего времени сотрудников. При разработке учтены вопросы шифрования данных и безопасности использования системы несколькими пользователями.
 
![Безымянный.png](https://bitbucket.org/repo/Kojzxn/images/1245741916-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
 
![Безымянный2.png](https://bitbucket.org/repo/Kojzxn/images/3268214721-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B92.png)
 
![Безымянный3.png](https://bitbucket.org/repo/Kojzxn/images/975450780-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B93.png)
 
![Безымянный4.png](https://bitbucket.org/repo/Kojzxn/images/3073340813-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B94.png)
 
![Безымянный5.png](https://bitbucket.org/repo/Kojzxn/images/3433605924-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B95.png)
 
![Безымянный6.png](https://bitbucket.org/repo/Kojzxn/images/3742040760-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B96.png)
 
![Безымянный7.png](https://bitbucket.org/repo/Kojzxn/images/3705823029-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B97.png)
 
![Безымянный8.png](https://bitbucket.org/repo/Kojzxn/images/2020192060-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B98.png)
 
![Безымянный9.png](https://bitbucket.org/repo/Kojzxn/images/4260511727-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B99.png)
 
![Безымянный11.png](https://bitbucket.org/repo/Kojzxn/images/3032932105-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B911.png)