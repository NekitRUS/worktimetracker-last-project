VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmFillTimesheet 
   Caption         =   "UserForm1"
   ClientHeight    =   6180
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8835
   OleObjectBlob   =   "frmFillTimesheet.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmFillTimesheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private curYear As String, curMonth As String
Public curSheet As Worksheet
Private controlsCollection As Collection
Private formWorker As Form
Private countDays As Integer
Public curDay As Integer
Public EnableEvents As Boolean

'==========================================================
'�������� ����� ��� ������� ���������� ������������� ������
'==========================================================
Public Sub SetMonth(ByVal year As String, ByVal month As String)

    curYear = year
    curMonth = month
    
End Sub

Private Sub fraContent_Click()

End Sub

'================================================================================================
'��������� ����� ���������� ������
'
'�������� ���� � ������� ��� ���������� ������, ���� ���� ����������� �� ������������ ����������,
'��������� ����������� �������� ���������� ����� ��� ��������� �������� ������������ �����
'================================================================================================
Private Sub UserForm_Activate()
    
    Dim book As Workbook
    Dim fileWorker As File
    
    On Error GoTo GlobalException
    
    Me.Enabled = False
    Me.Caption = "����������, ���������! ��� �������� ������ �������..."
    
    EnableEvents = True
    Set controlsCollection = New Collection
    Set fileWorker = New File
    Set formWorker = New Form
    Set book = fileWorker.OpenWorkbookWithoutExtension(APP_DATA_PATH & frmMain.currentUser.name & _
                                                        Application.PathSeparator & _
                                                        curYear & Application.PathSeparator & _
                                                        "������_" & frmMain.currentUser.name & _
                                                        "(" & curYear & ")", _
                                                        frmMain.currentUser.Password)
    If (book Is Nothing) Then
        MsgBox "�� ������� ������� ���� � �������!", vbOKOnly + vbCritical, APP_NAME
        Call cmdBack_Click
        Exit Sub
    End If
    If (Not (fileWorker.IsSheetExist(curMonth, book, curSheet))) Then
        MsgBox "�� ������� ������� ���� � �������!", vbOKOnly + vbCritical, APP_NAME
        Call cmdBack_Click
        Exit Sub
    End If
    If (Not (fileWorker.IsSheetValid(curSheet))) Then
        MsgBox "��������� ������ ����������!" & vbCrLf & _
                "���������� ����������� ������.", vbOKOnly + vbCritical, APP_NAME
        Call cmdBack_Click
        Exit Sub
    End If
    
    curDay = 41
    Me.fraContent.Visible = False
    Call AddControls
    Me.fraContent.Visible = True
    
    countDays = CountOfDays
    Dim temp As Integer
    temp = Day(Date)
    If (temp > countDays) Then
        curDay = 1
    Else
        curDay = temp
    End If
    
    For temp = 1 To countDays
        Me.cboDay.AddItem temp
    Next temp
    
    With Me
        .cboDay.Value = curDay
        .Caption = "������ �� " & LCase(curMonth) & " " & LCase(curYear) & "�."
        .Enabled = True
        .Enabled = True 'Known mystic bug fix
    End With
    curSheet.Activate
    
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'==========================================================================================
'���������� ������� ������� ������� � ��������� ���� ������������ ����� ��� ���� ����������
'
'����������� ����������� �� ���� ������ �������� ������
'==========================================================================================
Private Sub txtHoursAll_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

    formWorker.OnlyDigits KeyAscii

End Sub

'=============================================================================================
'���������� ������� ��������� �������� � ��������� ���� ������������ ����� ��� ���� ����������
'
'����������� ����������� �� ���� ����������� ����������� ��������,
'����� �������� ������������ �� ��� �������� ������������ �����
'=============================================================================================
Private Sub txtHoursAll_Change()

    If (Not (EnableEvents)) Then
        Exit Sub
    End If
    
    EnableEvents = False
    formWorker.MaxValue 24, Me.txtHoursAll
    EnableEvents = True
    If (IsNumeric(Me.txtHoursAll.Value)) Then
        EnableEvents = False
        Me.spnToAll.Value = Me.txtHoursAll.Value
        EnableEvents = True
        Dim control As MyControl
        For Each control In controlsCollection
            control.spnField.Value = 0
            control.spnField.Value = Me.txtHoursAll.Value
        Next control
        Set control = Nothing
    End If

End Sub

'============================================================================
'���������� ������� ��������� �������� ������������ ����� ��� ���� ����������
'
'����� �������� �������� ������������ � ��������������� ��������� ���� �����
'============================================================================
Private Sub spnToAll_Change()

    Me.txtHoursAll.Text = Me.spnToAll.Value

End Sub

'=========================================================================
'���������� ������� ��������� �������� � ���� �� ������� ������ ��� ������
'
'���������� ����� ������� �� ������� �����
'=========================================================================
Private Sub cboDay_Change()

    On Error GoTo GlobalException

    curDay = CInt(Me.cboDay.Value)
    
    EnableEvents = False
    Me.spnToAll.Value = 0
    Me.txtHoursAll.Value = 0
    EnableEvents = True
    
    Dim control As MyControl
    Dim temp As String
    For Each control In controlsCollection
        With control.txtField3
            temp = Trim(LCase(curSheet.Cells(Right(.name, Len(.name) - InStrRev(.name, "_")), curDay + 3).Text))
            If (temp = "��") Then
                control.optField2.Value = True
            ElseIf (temp = "��") Then
                control.optField3.Value = True
            ElseIf (temp = "��") Then
                control.optField4.Value = True
            ElseIf (temp = "�") Then
                control.optField5.Value = True
            Else
                control.optField1.Value = True
                .Text = temp
            End If
        End With
    Next control
    Set control = Nothing
    
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'===============================================
'���������� ������� ����� �� ������ "����������"
'
'���������� ��������� �������� ������� �� �����
'===============================================
Private Sub cmdPrev_Click()

    If (curDay > 1) Then
        curDay = curDay - 1
        Me.cboDay.Value = curDay
    End If

End Sub

'===============================================
'���������� ������� ����� �� ������ "���������"
'
'���������� ��������� �������� ������� �� �����
'===============================================
Private Sub cmdNext_Click()
    
    If (curDay < countDays) Then
        curDay = curDay + 1
        Me.cboDay.Value = curDay
    End If
    
End Sub

'=======================================================
'���������� ������� ����� �� ������ "���������"
'
'������������ ������� ������������ ����� � ������ ������
'=======================================================
Private Sub cmdSum_Click()

    On Error GoTo GlobalException
    
    Dim monthWorker As timesheet
    Set monthWorker = New timesheet
    monthWorker.Init curSheet
    monthWorker.SummUpHours
    Set monthWorker = Nothing
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'=======================================================
'���������� ������� ����� �� ������ "�����"
'
'����� ��� ���������� ������ ����������� �� ������,
'���������� �������� ������� �����
'=======================================================
Private Sub cmdBack_Click()

    Unload Me
    frmMain.Show
    
End Sub

'=============================================================
'���������� ������� ����� �� ������ "�����"
'
'����� ��� ���������� ������ � ��������� ����������� �� ������
'=============================================================
Private Sub cmdExit_Click()

    Unload Me
    End
    
End Sub

'==========================================================================================================
'���������� ������� �������� ����� ���������� ������
'
'�������, ���������������� ��� ������ � ������, ������ � ������� ��������� ���������� ����������� �� ������
'==========================================================================================================
Private Sub UserForm_Terminate()

    Set curSheet = Nothing
    Set controlsCollection = Nothing
    Set formWorker = Nothing

End Sub

'=================================================================================
'�������� ����� ��� �������� ������������ ���������� ��������� ���������� �� �����
'=================================================================================
Private Sub AddControls()
    
    Dim monthWorker As timesheet
    Dim control As MyControl
    Dim i As Long, topPosition As Long
    Dim step As Integer
    Dim curRow As String, curCaption As String
    Set monthWorker = New timesheet
    monthWorker.Init curSheet
    topPosition = 50
    For i = 5 To monthWorker.LastUsedRow(2)
        DoEvents
        curCaption = Trim(curSheet.Cells(i, 2).Value)
        If (curCaption <> vbNullString) Then
        
            curRow = CStr(i)
            Set control = New MyControl
            Set control.txtField1 = Me.fraContent.Controls.Add("Forms.TextBox.1", "txtNum_" & curRow)
            With control.txtField1
                .Left = 5
                .Top = topPosition
                .Width = 20
                .Height = 20
                .Font.size = 10
                .Locked = True
                .BackColor = &H8000000F
                .Text = Trim(curSheet.Cells(i, 1).Value)
                .SelectionMargin = False
                .MousePointer = fmMousePointerArrow
                .TabStop = False
            End With
            Set control.txtField2 = Me.fraContent.Controls.Add("Forms.TextBox.1", "txtName_" & curRow)
            With control.txtField2
                .Left = 30
                .Top = topPosition
                .Width = 120
                .Height = 20
                .Font.size = 10
                .Locked = True
                .BackColor = &H8000000F
                .Text = curCaption
                .SelectionMargin = False
                .MousePointer = fmMousePointerArrow
                .TabStop = False
                .TextAlign = fmTextAlignCenter
            End With
            Set control.txtField3 = Me.fraContent.Controls.Add("Forms.TextBox.1", "txtHours_" & curRow)
            With control.txtField3
                .Left = 156
                .Top = topPosition
                .Width = 25
                .Height = 20
                .Font.size = 10
                .MousePointer = fmMousePointerIBeam
                .Tag = 0
            End With
            Set control.spnField = Me.fraContent.Controls.Add("Forms.SpinButton.1", "spn_" & curRow)
            With control.spnField
                .Left = 181
                .Top = topPosition
                .Width = 13
                .Height = 20
                .Min = 0
                .max = 24
                .SmallChange = 1
                .Value = 0
            End With
            Set control.fraField = Me.fraContent.Controls.Add("Forms.Frame.1", "fra_" & curRow)
            With control.fraField
                .Left = 210
                .Top = topPosition
                .Width = 90
                .Height = 100
            End With
            Set control.optField1 = Me.fraContent.Controls("fra_" & _
                                    curRow).Controls.Add("Forms.OptionButton.1", "optNone_" & curRow)
            With control.optField1
                .Caption = "���"
                .Value = True
            End With
            Set control.optField2 = Me.fraContent.Controls("fra_" & _
                                    curRow).Controls.Add("Forms.OptionButton.1", "optVacation_" & curRow)
            With control.optField2
                .Caption = "������"
                .Top = 15
                .ForeColor = &HFF0000
            End With
            Set control.optField3 = Me.fraContent.Controls("fra_" & _
                                    curRow).Controls.Add("Forms.OptionButton.1", "optHooky_" & curRow)
            With control.optField3
                .Caption = "������"
                .Top = 60
                .ForeColor = &HFF
            End With
            Set control.optField4 = Me.fraContent.Controls("fra_" & _
                                    curRow).Controls.Add("Forms.OptionButton.1", "optVacationNonPaid_" & curRow)
            With control.optField4
                .Caption = "��������������" & vbLf & "������"
                .Top = 30
                .Height = 30
                .ForeColor = &HC0C000
            End With
            Set control.optField5 = Me.fraContent.Controls("fra_" & _
                                    curRow).Controls.Add("Forms.OptionButton.1", "optDisability_" & curRow)
            With control.optField5
                .Caption = "����������"
                .Top = 75
                .ForeColor = &H8000&
            End With
            topPosition = topPosition + 110
            controlsCollection.Add Item:=control, Key:=curRow
            
        End If
    Next i
    Me.fraContent.ScrollHeight = Me.fraContent.InsideHeight * (controlsCollection.Count + 1) / 2.05
    
    Set monthWorker = Nothing
    Set control = Nothing

End Sub

'============================================================
'�������� ����� ��� �������� ���������� ���� �� ������� �����
'============================================================
Private Function CountOfDays() As Integer

    CountOfDays = 28
    On Error GoTo Exception
    Dim i As Integer
    For i = 29 To 31
        If (CInt(Trim(curSheet.Cells(4, i + 3).Value)) = i) Then
            CountOfDays = i
        End If
    Next i
    Exit Function
    
Exception:
    Exit Function
    
End Function
