VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmAuth 
   Caption         =   "���� � ��������� ""��������"""
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7935
   OleObjectBlob   =   "frmAuth.frx":0000
End
Attribute VB_Name = "frmAuth"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private authWorker As Authentication

'==================================
'������������� ����� ��������������
'
'���������� ������ �������
'==================================
Private Sub UserForm_Initialize()

    On Error GoTo GlobalException

    Set authWorker = New Authentication
    
    Dim listOfUsers As Collection
    authWorker.SortUsersOnSheet ThisWorkbook.Sheets(1)
    Set listOfUsers = authWorker.CreateListOfUsers(ThisWorkbook.Sheets(1))
    
    Dim i As Long
    For i = 1 To listOfUsers.Count
        Me.cboLogin.AddItem listOfUsers(i).name, i - 1
    Next i
    
    With Me
        .Top = (Application.Top + Application.Height - .Height) / 2
        .Left = (Application.Left + Application.Width - .Width) / 2
    
        .lblError.Visible = False
        .chkConnect.Value = False
        .cmdConnect.Enabled = False
    End With
    
    Set listOfUsers = Nothing
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'=================================================================================
'���������� ��������� �������� � ���������� ������ �������������
'
'���� ������������ ��������� ���� ������ ���, �� ��� ������������ ��������� ������
'=================================================================================
Private Sub cboLogin_Change()
    
    On Error GoTo GlobalException
    
    With Me.lblError
        .Visible = False
        If (authWorker.GetUser(Me.cboLogin.Value).Password = vbNullString) Then
            .Caption = "�� ����������� ���������� �������." & vbCrLf & _
                        "��������� ������ ����� �������� ��� ����������� ������."
            .Visible = True
        End If
    End With
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'==================================================================
'���������� ������� ��������� ������������� "����������� ���������"
'
'� ����������� �� ������
'������������ � ���������� ��������� ��� ���������� ������ �������
'==================================================================
Private Sub chkConnect_Change()

    If (Me.chkConnect.Value) Then
        Me.Height = 360
    Else
        Me.Height = 164
    End If

End Sub

'=======================================================================
'���������� ������� ��������� �������� � ��������� ���� ��� ����� ������
'
'���� �������� �� �������� ����������� ���������,
'�� � ���������� ���� ��������� ��������� ������� ������
'=======================================================================
Private Sub txtPassword_Change()

    With Me.txtPassword
        .BorderStyle = fmBorderStyleNone
        .BorderColor = &H80000006
        .SpecialEffect = fmSpecialEffectSunken
    End With

End Sub

'=================================================================
'���������� ������� ������ ������ ���������� ���� ��� ����� ������
'
'� ���������� ���� ��������� ��������� ������� ������
'=================================================================
Private Sub txtPassword_Exit(ByVal Cancel As MSForms.ReturnBoolean)

    With Me.txtPassword
        .BorderStyle = fmBorderStyleNone
        .BorderColor = &H80000006
        .SpecialEffect = fmSpecialEffectSunken
    End With

End Sub
'=======================================================================================================
'���������� ������� ������� �� ������ "����"
'
'���� ������������ �� ���� ������, �� ��� ���� �������������� ������� ������;
'��� ������ ����� ������������ � ��������� ���������� ����������� ������ � ����������� ���� � ���������;
'���� ������������ ��� ��� ��������������� � ���� ������ ������, �� ��������� ���� � ���������
'=======================================================================================================
Private Sub cmdLogin_Click()

    Dim pass As String
    Dim user As Person

    On Error GoTo GlobalException

    pass = Me.txtPassword.Text
    If ((pass = vbNullString) Or (Me.cboLogin.ListIndex = -1)) Then
        If (pass = vbNullString) Then
            With Me.txtPassword
                .Value = vbNullString
                .BorderStyle = fmBorderStyleSingle
                .BorderColor = vbRed
                .SpecialEffect = fmSpecialEffectFlat
                .SetFocus
            End With
        End If
        Exit Sub
    End If
    
    Set user = authWorker.GetUser(Me.cboLogin.Text)
    
    If (Me.lblError.Visible) Then
        If (authWorker.RegisterUser(user.name, pass, ThisWorkbook.Sheets(1))) Then
            Unload Me
            frmMain.SetUser user.name, pass
            Set user = Nothing
            frmMain.Show
            Exit Sub
        Else
            MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
            End
        End If
    End If
    
    If (authWorker.Verify(pass, user)) Then
        Unload Me
        frmMain.SetUser user.name, pass
        Set user = Nothing
        frmMain.Show
        Exit Sub
    Else
        With Me.txtPassword
            .Value = vbNullString
            .SetFocus
        End With
        MsgBox "�������� ������!", vbOKOnly + vbExclamation, APP_NAME
    End If
    
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'======================================================================
'���������� ������� ������� �� ��������� ���� ��� ����������� ���� � ��
'
'������������ ������� �� ������ "�����"
'======================================================================
Private Sub txtPath_MouseDown( _
                            ByVal Button As Integer, _
                            ByVal Shift As Integer, _
                            ByVal X As Single, _
                            ByVal Y As Single _
                            )

    If (Button = 1) Then
        Call cmdPath_Click
    End If

End Sub

'============================================
'���������� ������� ������� �� ������ "�����"
'
'����������� ���� ������ �����
'============================================
Private Sub cmdPath_Click()
   
    With Application.FileDialog(msoFileDialogOpen)
        .AllowMultiSelect = False
        .Title = "�������� ���� � ����� ������ ��������� �������"
        .InitialFileName = APP_DATA_PATH
        .Filters.Clear
        .Filters.Add Description:="������� Excel", Extensions:="*.xls, *.xlsx, *.xlsm, *.xlsb"
        If (.Show <> 0) Then
            Me.txtPath.Text = .SelectedItems(1)
            Me.cmdConnect.Enabled = True
        End If
    End With

End Sub

'==============================================================
'���������� ������� ������� �� ������ "�������� ������"
'
'������ ������������������ ������������� �����������;
'���������� ���������� �������� �����, ��������� �������������;
'����� ��������������� � ����� ������� �������������
'==============================================================
Private Sub cmdConnect_Click()

    Dim alerts As Boolean, updateScreen As Boolean, success As Boolean, activated As Boolean
    Dim wb As Workbook
    Dim fileWorker As File
    
    On Error GoTo GlobalException
    
    alerts = Application.DisplayAlerts
    updateScreen = Application.screenUpdating
    Application.DisplayAlerts = False
    Application.screenUpdating = False
    Set fileWorker = New File
    
    Set wb = fileWorker.OpenWorkbook(Me.txtPath, Me.txtPasswordPath, activated)
    If (wb Is Nothing) Then
        MsgBox "�� ������� ������� �����!", vbOKOnly + vbExclamation, APP_NAME
        GoTo ResetSettings
    End If
    success = authWorker.UpdateListOfUsers(ThisWorkbook.Sheets(1), wb)
    If (Not (activated)) Then
        wb.Close savechanges:=False
    End If
    
ResetSettings:
    Application.DisplayAlerts = alerts
    Application.screenUpdating = updateScreen
    Set wb = Nothing
    Set fileWorker = Nothing
    
    If (success) Then
        Unload Me
        frmAuth.Show
    Else
        MsgBox "������ �� ��������.", vbOKOnly + vbExclamation, APP_NAME
    End If
    
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'====================================================================
'���������� ������� �������� ����� ��������������
'
'������, ���������������� ��� ������ � ������, ����������� �� ������
'====================================================================
Private Sub UserForm_Terminate()

    Set authWorker = Nothing
    
End Sub
