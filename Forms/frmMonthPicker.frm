VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmMonthPicker 
   Caption         =   "����� ������"
   ClientHeight    =   3135
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8370
   OleObjectBlob   =   "frmMonthPicker.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmMonthPicker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private dates As Collection
Private forTimesheet As Boolean, forReport As Boolean

'=======================================================================================================
'��������� ��� ������� ������ ������ ����� ������ ������ (��� ���������� ������ ��� ��� �������� ������)
'=======================================================================================================
Public Sub SetMod(ByVal timesheet As Boolean, ByVal Report As Boolean)

    forTimesheet = timesheet
    forReport = Report
    
End Sub

'===========================================================================
'������������� ����� ��� ������ ������
'
'����� �������� ������������ ����������� �� ������� ������ �������,
'�� ����������� ������������ ������������ ���������� ������� ������� � �����
'===========================================================================
Private Sub UserForm_Initialize()
    
    On Error Resume Next
    
    With Me.lblAlert
        .Caption = "����� ������� �� ���� ��� �� �������!" & vbCrLf & _
                            "���������� ������� ������� ������."
        .Visible = False
    End With
    Me.cmdOK.Enabled = False
    
    Dim year As Integer
    year = DatePart("yyyy", Date)
    With Me.cboYear
        .AddItem CStr(year - 1)
        .AddItem CStr(year)
        .AddItem CStr(year + 1)
    End With
    
    Dim update As Boolean
    update = Application.screenUpdating
    Application.screenUpdating = False
    Dim fileWorker As File
    Set fileWorker = New File
    Set dates = fileWorker.ListOfTimesheets(CStr(year), frmMain.currentUser)
        
    Me.cboYear.ListIndex = 1
    
    Application.screenUpdating = update
    Set fileWorker = Nothing
    
End Sub

'=================================================================================
'���������� ������� ��������� �������� � ���� �� ������� �����
'
'���� �� ������� ������� ����������� ���������������� ������� �� ������ ������ ���
'=================================================================================
Private Sub cboYear_Change()

    On Error GoTo Exception
    
    Me.cboMonth.Clear
    Dim i As Integer
    For i = 1 To dates(Me.cboYear.Text).Count
        Me.cboMonth.AddItem dates(Me.cboYear.Text)(i)
    Next i
    If (dates(Me.cboYear.Text).Count > 0) Then
        Me.cboMonth.ListIndex = 0
    End If
    Call cboMonth_Change
    Exit Sub

Exception:
    Exit Sub
    
End Sub

'================================================================================================
'���������� ������� ��������� �������� � ���� �� ������� �������
'
'���� ���������� ��������� �������� ������, �� ������ "��" ���������� ��������,
'���� �������� �������� ������, �� �������������� �� ���������� ������ ������� ���������� �������
'================================================================================================
Private Sub cboMonth_Change()

    If ((Me.cboMonth.ListIndex = -1) Or (Me.cboMonth.Text = vbNullString)) Then
        Me.lblAlert.Visible = True
        Me.cmdOK.Enabled = False
    Else
        Me.lblAlert.Visible = False
        Me.cmdOK.Enabled = True
    End If

End Sub

'==================================================================================================
'���������� ������� ����� �� ������ "��"
'
'� ����������� �� �������� ������ ������ ����� ������ ������
'���� ������������ ��������� ��������� ����� ���������� ������, � ����� ��� ������������ �� ������,
'���� �������� ������ �� ����� ������ ���������� ������ ����������� � ������ ������
'==================================================================================================
Private Sub cmdOK_Click()

    On Error GoTo GlobalException

    If (forTimesheet) Then
        frmFillTimesheet.SetMonth Me.cboYear.Text, Me.cboMonth.Text
        Unload Me
        frmFillTimesheet.Show
    ElseIf (forReport) Then
        Dim wasActivated As Boolean, alerts As Boolean, updateScreen As Boolean
        Dim book As Workbook
        Dim doc As Object
        Dim curSheet As Worksheet
        Dim fileWorker As File
        Dim reportWorker As Report
        Set fileWorker = New File
        Set reportWorker = New Report
        
        alerts = Application.DisplayAlerts
        updateScreen = Application.screenUpdating
        Application.DisplayAlerts = False
        Application.screenUpdating = False
           
        Set book = fileWorker.OpenWorkbookWithoutExtension(APP_DATA_PATH & frmMain.currentUser.name & _
                                                        Application.PathSeparator & _
                                                        Me.cboYear.Text & Application.PathSeparator & _
                                                        "������_" & frmMain.currentUser.name & _
                                                        "(" & Me.cboYear.Text & ")", _
                                                        frmMain.currentUser.Password)
        If (book Is Nothing) Then
            MsgBox "�� ������� ������� ���� � �������!", vbOKOnly + vbCritical, APP_NAME
            Application.DisplayAlerts = alerts
            Application.screenUpdating = updateScreen
            Call cmdBack_Click
            Exit Sub
        End If
        If (Not (fileWorker.IsSheetExist(Me.cboMonth.Text, book, curSheet))) Then
            MsgBox "�� ������� ������� ���� � �������!", vbOKOnly + vbCritical, APP_NAME
            Application.DisplayAlerts = alerts
            Application.screenUpdating = updateScreen
            Call cmdBack_Click
            Exit Sub
        End If
        Set doc = fileWorker.OpenDocumentWithoutExtension(APP_DATA_PATH & frmMain.currentUser.name & _
                                                        Application.PathSeparator & _
                                                        Me.cboYear.Text & Application.PathSeparator & _
                                                        "������" & Application.PathSeparator & _
                                                        Me.cboMonth.Text, wasActivated)
        If (Not (doc Is Nothing)) Then
            If (MsgBox("����� �� ������ ����� ��� ����������." & vbCrLf & _
                        "������ ������������ ������?", vbQuestion + vbYesNo, APP_NAME) = vbYes) Then
                Dim path As String
                path = doc.fullName
                doc.Close savechanges:=True
                SetAttr path, vbNormal
                Kill path
            Else
                If (Not (wasActivated)) Then
                    doc.Close savechanges:=True
                End If
                Application.DisplayAlerts = alerts
                Application.screenUpdating = updateScreen
                Set book = Nothing
                Set doc = Nothing
                Set curSheet = Nothing
                Set fileWorker = Nothing
                Set reportWorker = Nothing
                Exit Sub
            End If
        End If
        reportWorker.CreateReport curSheet, Me.cboMonth.Text, Me.cboYear.Text, frmMain.currentUser.name
        Application.DisplayAlerts = alerts
        Application.screenUpdating = updateScreen
        Set book = Nothing
        Set doc = Nothing
        Set curSheet = Nothing
        Set fileWorker = Nothing
        Set reportWorker = Nothing
    End If
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'==============================================
'���������� ������� ����� �� ������ "�����"
'
'����� ��� ������ ������ ����������� �� ������,
'���������� �������� ������� �����
'==============================================
Private Sub cmdBack_Click()

    Unload Me
    frmMain.Show
    
End Sub

'==================================================================
'���������� ������� �������� ����� ������ ������
'
'������, ���������������� ��� ������ � ������ ����������� �� ������
'==================================================================
Private Sub UserForm_Terminate()

    Set dates = Nothing

End Sub
