VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmNewTimesheet 
   Caption         =   "�������� ������ ������"
   ClientHeight    =   5250
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7935
   OleObjectBlob   =   "frmNewTimesheet.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmNewTimesheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Private monthWorker As timesheet

'==============================================
'������������� ����� ��� �������� ������ ������
'
'���������� ������� ������� � �����
'==============================================
Private Sub UserForm_Initialize()

    Set monthWorker = New timesheet
    
    Dim months As Variant
    months = Array("������", "�������", "����", "������", "���", "����", _
                    "����", "������", "��������", "�������", "������", "�������")
    Me.cboMonth.list = months

    Me.cboMonth.ListIndex = month(now) - 1
    
    Dim year As Integer
    year = DatePart("yyyy", Date)
    Me.cboYear.AddItem CStr(year - 1)
    Me.cboYear.AddItem CStr(year)
    Me.cboYear.AddItem CStr(year + 1)
    
    With Me
        .cboYear.ListIndex = 1
        .chkLoadWorkers.Value = False
        .txtTime.Text = Me.spnTime.Value
    End With
    
End Sub

'============================================================================
'���������� ������� ��������� �������� ����������� ���������� ������ ��������
'
'����� �������� �������� ������������ � ��������� ���� �����
'============================================================================
Private Sub spnTime_Change()

    Me.txtTime.Text = Me.spnTime.Value
    
End Sub

'===========================================================================
'���������� ������� ����� �� ����� �������
'
'��������� ���������� � �������� ��������������� ����������� ������� �������
'===========================================================================
Private Sub lblHelp_Click()

    MsgBox "��� �������� ������ ����������, " & _
            "����� ���� � �������� �������� ��������� � ����� ������ ���������� � ���� �������� " & _
            Chr(34) & "�����" & Chr(34) & ".", vbOKOnly + vbInformation, APP_NAME
            
End Sub

'============================================================================================
'���������� ������� ����� �� ������ "��"
'
'� ����� ������� �������� ������������ ���� ����������� ����� ������� ���� ���������� ������,
'���� ������������ ������������ ������ �� ������������ �����;
'����� ���� �������������,
'������������� ��������� ��� � ����������� �� ��������� ����� (� ���������� ��� ���)
'============================================================================================
Private Sub cmdOK_Click()

    Dim alerts As Boolean, updateScreen As Boolean, activated As Boolean
    Dim year As Integer, month As Integer
    Dim userPath As String
    Dim dialogResult As VbMsgBoxResult
    Dim wb As Workbook
    Dim fileWorker As File
    
    On Error GoTo GlobalException
    
    alerts = Application.DisplayAlerts
    updateScreen = Application.screenUpdating
    Application.DisplayAlerts = False
    Application.screenUpdating = False
    year = CInt(Me.cboYear.Text)
    month = Me.cboMonth.ListIndex + 1
    Set fileWorker = New File
    userPath = APP_DATA_PATH & fileWorker.CorrectPath(frmMain.currentUser.name) & _
                Application.PathSeparator & CStr(year) & Application.PathSeparator
    Set wb = fileWorker.OpenWorkbookWithoutExtension( _
                                                    userPath & "������_" & _
                                                    fileWorker.CorrectPath(frmMain.currentUser.name) & "(" & _
                                                    CStr(year) & ")", frmMain.currentUser.Password, activated _
                                                    )
    If (wb Is Nothing) Then
        Dim sheetsCount As Long
        sheetsCount = Application.SheetsInNewWorkbook
        Application.SheetsInNewWorkbook = 1
        Set wb = Workbooks.Add
        Application.SheetsInNewWorkbook = sheetsCount
        fileWorker.CreateDirectory userPath
        wb.Sheets(1).name = Me.cboMonth.Text
        fileWorker.InsertWorkbookEventHandlersAndModules wb
        If (EXCEL_VERSION >= 12#) Then
            wb.SaveAs Filename:=userPath & "������_" & fileWorker.CorrectPath(frmMain.currentUser.name) & _
                                "(" & CStr(year) & ").xlsm", _
                        FileFormat:=52, _
                        Password:=frmMain.currentUser.Password
                        '52 = xlOpenXMLWorkbookMacroEnabled (Excel 2003 unknown constant fix)
        Else
            wb.SaveAs Filename:=userPath & "������_" & fileWorker.CorrectPath(frmMain.currentUser.name) & _
                                "(" & CStr(year) & ").xls", _
                        FileFormat:=xlWorkbookNormal, _
                        Password:=frmMain.currentUser.Password
        End If
        monthWorker.Init wb.Sheets(1)
    Else
        If (fileWorker.IsSheetExist(Me.cboMonth.Text, wb)) Then
            If (MsgBox("������ �� ������ ����� ��� ����������." & vbCrLf & _
                        "������ ������������ ������?", vbQuestion + vbYesNo, APP_NAME) = vbYes) Then
                monthWorker.Init wb.Sheets(Me.cboMonth.Text)
            Else
                If (Not (activated)) Then
                    wb.Close savechanges:=True
                End If
                Set wb = Nothing
                Set fileWorker = Nothing
                Application.DisplayAlerts = alerts
                Application.screenUpdating = updateScreen
                Exit Sub
            End If
        Else
            Dim sheet As Worksheet
            Set sheet = fileWorker.AddSheet(Me.cboMonth.Text, wb)
            If (Not (sheet Is Nothing)) Then
                monthWorker.Init sheet
            End If
        End If
        fileWorker.InsertWorkbookEventHandlersAndModules wb
    End If
    
    If (monthWorker.ProcessingSheet Is Nothing) Then
        GoTo GlobalException
    End If
    
    fileWorker.InsertSheetEventHandlers monthWorker.ProcessingSheet
    Set wb = Nothing
    
    If (Me.chkLoadWorkers.Value) Then
    
        Dim success As Boolean
        
        success = False
        
        Set wb = fileWorker.OpenWorkbookWithoutExtension( _
                                                        APP_DATA_PATH & "�����", _
                                                        frmMain.currentUser.Password, _
                                                        activated _
                                                        )
        If (wb Is Nothing) Then
            GoTo Result
        End If
        success = monthWorker.LoadListOfWorkers(frmMain.currentUser.name, wb)
        If (Not (activated)) Then
            wb.Close savechanges:=True
        End If
        
Result:
        Set wb = Nothing
        Set fileWorker = Nothing
        
        If (Not success) Then
            MsgBox "������ ������� �� ��������.", vbOKOnly + vbExclamation, APP_NAME
        End If
    
    End If
    
    With monthWorker
        .DecorateAndPrepareForPrint month, Me.cboMonth.Text, year, frmMain.currentUser.name
        
        If (Me.optManual.Value) Then
            .ColorHolidays month, year
        ElseIf (Me.optInternet.Value) Then
            On Error GoTo ManualHolidays
            .ColorHolidaysWithInternet month, _
                                            Me.cboMonth.Text, _
                                            year, _
                                            CInt(Me.txtTime.Text)
            On Error GoTo GlobalException
        End If
    End With
    
    With monthWorker.ProcessingSheet
        .Activate
        CallByName .Parent, "Start", VbMethod, .name 'Excel not firing event fix
    End With
    
    MsgBox "������ ������� ���������!", vbInformation + vbOKOnly, APP_NAME
    Application.DisplayAlerts = alerts
    Application.screenUpdating = updateScreen
    Call cmdBack_Click
    Exit Sub
    
ManualHolidays:
    MsgBox Err.Description & vbCrLf & vbCrLf & "����� ��������� ������ ������� � �����������!", _
            vbCritical + vbOKOnly, "������ �����������"
    monthWorker.ColorHolidays month, year
    With monthWorker.ProcessingSheet
        .Activate
        CallByName .Parent, "Start", VbMethod, .name 'Excel not firing event fix
    End With
    MsgBox "������ ������� ���������!", vbInformation + vbOKOnly, APP_NAME
    Application.DisplayAlerts = alerts
    Application.screenUpdating = updateScreen
    Call cmdBack_Click
    Exit Sub
    
GlobalException:
    MsgBox GLOBAL_ERR_DESCRIPTION, vbOKOnly + vbCritical, APP_NAME
    End
    
End Sub

'=======================================================
'���������� ������� ����� �� ������ "�����"
'
'����� ��� �������� ������ ������ ����������� �� ������,
'���������� �������� ������� �����
'=======================================================
Private Sub cmdBack_Click()

    Unload Me
    frmMain.Show
    
End Sub

'==================================================================
'���������� ������� ����� �� ������ "�����"
'
'����� ��� �������� ������ ������ � ��������� ����������� �� ������
'==================================================================
Private Sub cmdExit_Click()

    Unload Me
    End
    
End Sub

'==================================================================
'���������� ������� �������� ����� ��� �������� ������ ������
'
'������, ���������������� ��� ������ � ������ ����������� �� ������
'==================================================================
Private Sub UserForm_Terminate()

    Set monthWorker = Nothing

End Sub
