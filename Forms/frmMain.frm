VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmMain 
   Caption         =   "��������� ""��������"""
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4965
   OleObjectBlob   =   "frmMain.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0

'=====================
'���������� ����������
'=====================
Public currentUser As Person

'==================================
'��������� ��� ������� ������������
'==================================
Public Sub SetUser(ByVal userName As String, ByVal userPass As String)

    currentUser.name = userName
    currentUser.Password = userPass
    
End Sub

'==================================================
'������������� ������� �����
'
'��������� ������ ��� ������ � ������� ������������
'==================================================
Private Sub UserForm_Initialize()

    Set currentUser = New Person

End Sub

'=========================================================================
'���������� ������� ����� �� ������ "����� ������"
'
'������� ����� ����������, �� ����� ��������� ����� �������� ������ ������
'=========================================================================
Private Sub cmdNew_Click()

    Me.Hide
    frmNewTimesheet.Show

End Sub

'================================================================
'���������� ������� ����� �� ������ "��������� ������"
'
'������� ����� ����������, �� ����� ��������� ����� ������ ������
'================================================================
Private Sub cmdFill_Click()

    Me.Hide
    frmMonthPicker.SetMod timesheet:=True, Report:=False
    frmMonthPicker.Show
    
End Sub

'================================================================
'���������� ������� ����� �� ������ "����� �����"
'
'������� ����� ����������, �� ����� ��������� ����� ������ ������
'================================================================
Private Sub cmdReport_Click()

    Me.Hide
    frmMonthPicker.SetMod timesheet:=False, Report:=True
    frmMonthPicker.Show
    
End Sub

'==========================================
'���������� ������� ����� �� ������ "�����"
'
'������� ����� ����������� �� ������
'==========================================
Private Sub cmdExit_Click()

    Unload Me
    Exit Sub

End Sub

'=================================================================================
'���������� ������� �������� ������� �����
'
'������, ���������������� ��� ������ � ������� ������������, ����������� �� ������
'=================================================================================
Private Sub UserForm_Terminate()

    Set currentUser = Nothing

End Sub
